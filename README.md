# Prueba de Primary

Prueba solicitada por primary

Introducción: 

En el presente documento pretende explicar cuales son las condiciones necesarias para llevar a cabo las pruebas solicitadas por la empresa Primary, como así también información adicional que puede resultar útil.

Precondiciones:

Al momento de realizar las pruebas deberá verificar que cuente con las siguientes herramientas:

Maven

Librerías

Controlador y explorador

Maven:

Deberá importar las dependencias de “uniRest” atreves de Maven para ejecutar las pruebas correspondientes al ejercicio numero 3. Estas dependencias se encuentran agregadas al archivo Pom.xml dentro del proyecto.

Librerías:

Deberá verificar que las librerías externas estén agregadas al proyecto. De ser necesario, tendrá que agregarlas al proyecto en el supuesto caso de que el IDE utilizado no las encuentre, las mismas se encuentran su disposicion en la carpeta llamada dependencias. Al momento del desarrollo se utilizó el IDE Eclipse versión: 2019-06 (4.12.0) Build id: 20190614-1200.

byte-buddy-1.8.15.jar

commons-exec-1.3.jar

guava-25.0-jre.jar

okhttp-3.11.0.jar

okio-1.14.0.jar

testng-6.7.jar

client-combined-3.141.59.jar

client-combined-3.141.59-sources.jar

Todas estas librerías son de uso libre.

Controlador y explorador:

Por último, para la ejecución de las pruebas deberá utilizar la versión del controlador WebDriver que se corresponda con su explorador.

Se le proporciona la versión del controlador que fue utilizada durante el desarrollo. La misma utiliza el explorador Google Chrome Versión 78.0.3904.108.

Puede descargar otros controladores desde la página oficial de selenium:

https://www.seleniumhq.org/download/

Casos de prueba:

Según las consignas descritas en el documento enviado por Primary, se detectaron las siguientes pruebas:

Caso 1: navegarPorCategoriasCondicionesCategoriaHogarYElectrodomesticosSubCategoriaClimatizacionResultadoOK

Caso 2: navegarPorCategoriasCondicionesCategoriaTecnologiaSubCategoriaCelularesYSmartphonesResultadoOK

Caso 3: navegarPorCategoriasCondicionesCategoriaHerramientsaEIndustriaSubCategoriaIndustriaTextilResultadoOK

Caso 4: navegarPorCategoriasCondicionesCategoriaJuguetesYBebesSubCategoriaCuartoDelBebeResultadoOK

Caso 5: navegarPorCategoriasCondicionesCategoriaBellezaYCuidadoPersonalSubCategoriaPerfumesImportadosResultadoError

Caso 6: busquedaDeProductoAleatorioCondicionesFiltradoPorCapitalResultadoOK

Caso 7: verificarCantidadDeStockCondicionesValidasResultadoOK

Caso 8: validarLimiteDelPaginadoCondicionesValidasResultadoOK

Caso 9: validarProductoConsultadoAlAzarCondicionesValidasResultadoOK

WorkFlow:

Las pruebas deben ser ejecutadas por medio de la clase AppTest incluida en el package Src/test/java/Main.Proyecto.

Aclaraciones:

Durante las pruebas realizadas se detectó que algunos nombres de productos no coinciden con los consultados en la api. Sin embargo, estas diferencias son lo suficientemente sutiles como para considerar a la prueba valida.

Acerca del desarrollador:

Nombre: Pino, Joel Alejandro

Email: Pinojoelalejandro@gmail.com

Telefono: 1552592254

Muchas gracias.