package Main.Proyecto;

import java.io.IOException;

import com.mashape.unirest.http.exceptions.UnirestException;

public class AppTest {

	public static void main(String[] args) throws IOException, UnirestException, InterruptedException {
		
		// Comienza el ejercicio 1
		Ejercicio1 ejercicio1 = new Ejercicio1();
		ejercicio1.navegarPorCategoriasCondicionesCategoriaHogarYElectrodomesticosSubCategoriaClimatizacionResultadoOK();
		ejercicio1.navegarPorCategoriasCondicionesCategoriaTecnologiaSubCategoriaCelularesYSmartphonesResultadoOK();
		ejercicio1.navegarPorCategoriasCondicionesCategoriaHerramientasEIndustriaSubCategoriaIndustriaTextilResultadoOK();
		ejercicio1.navegarPorCategoriasCondicionesCategoriaJuguetesYBebesSubCategoriaCuartoDelBebeResultadoOK();
		ejercicio1.navegarPorCategoriasCondicionesCategoriaBellezaYCuidadoPersonalSubCategoriaPerfumesImportadosResultadoError();
		// Comienza el ejercicio 2
		Ejercicio2 ejercicio2 = new Ejercicio2();
		ejercicio2.busquedaDeProductoAleatorioCondicionesFiltradoPorCapitalFederalResultadoOK();
		// Comienza el ejercicio 3
		Ejercicio3 ejercicio3 = new Ejercicio3();
		ejercicio3.validarLimiteDelPaginadoCondicionesValidasResultadoOK();
		ejercicio3.verificarCantidadDeStockCondicionesValidasResultadoOK();
		ejercicio3.validarProductoConsultadoAlAzarCondicionesValidasResultadoOK();
	}

}
