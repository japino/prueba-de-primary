/*************************************************************Prueba*********************************************************************
*Desarrollador: Joel Alejandro Pino
*
*Ultima Modificacion: 20/11/2019
*
*****************************************************************************************************************************************/
package Main.Proyecto;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.mashape.unirest.http.exceptions.UnirestException;

import Entidades.ApiMercadoLibre;

/******************************************************* Ejercicio 3**********************************************************************

Realizar la b�squeda de un producto existente y validar sobre la respuesta
(response de b�squeda) los siguientes conceptos:

* Total de productos encontrados
* Cantidad de productos devueltos y que esta no exceda el l�mite del paginado

De los resultados obtenidos en el punto anterior, elegir un id al azar, obtener su
detalle, y sobre este validar que los atributos coincidan con su equivalente en el
response de b�squeda de producto. Los atributos a validar son:

* Titulo
* Precio
* Acepta MercadoPago
* Moneda
* Env�o gratis

 ************************************************************* Test3 ***********************************************************************/
public class Ejercicio3 {

	@Test
	public void verificarCantidadDeStockCondicionesValidasResultadoOK() throws UnirestException, InterruptedException {
		String localDir = System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver", localDir + "\\dependencias\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		ApiMercadoLibre apiMercadoLibre = new ApiMercadoLibre();
		boolean validacion = apiMercadoLibre.verificarCantidadDeStock("https://api.mercadolibre.com/sites/MLA/search?q=motorola%20g7", "motorola g7", driver);
		Assert.assertTrue(validacion);
		if (validacion) {
			System.out.println("La validacion del producto selecionado aleatoriamente finalizo sin errores.");
		}
	}

	@Test
	public void validarLimiteDelPaginadoCondicionesValidasResultadoOK() throws UnirestException {
		ApiMercadoLibre apiMercadoLibre = new ApiMercadoLibre();
		boolean validacion = apiMercadoLibre.validarLimiteDelPaginado("https://api.mercadolibre.com/sites/MLA/search?q=motorola%20g6");
		Assert.assertTrue(validacion);
		if (validacion) {
			System.out.println("La validacion del limite de paginado finalizo sin errores.");
		}
	}

	@Test
	public void validarProductoConsultadoAlAzarCondicionesValidasResultadoOK() throws UnirestException, InterruptedException {
		String localDir = System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver", localDir + "\\dependencias\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		ApiMercadoLibre apiMercadoLibre = new ApiMercadoLibre();
		apiMercadoLibre.validarProductoConsultadoAlAzar("https://api.mercadolibre.com/sites/MLA/search?q=Motorola%20G6", driver);
	}

}
