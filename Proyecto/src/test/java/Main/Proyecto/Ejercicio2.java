/*************************************************************Prueba*********************************************************************
*Desarrollador: Joel Alejandro Pino
*
*Ultima Modificacion: 20/11/2019
*
*****************************************************************************************************************************************/
package Main.Proyecto;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import Entidades.MercadoLibre;

/*******************************************************Ejercicio 2**********************************************************************
Ingresar a una categoría a elección (que sea lo suficientemente común para que el
listado de productos que contenga sea amplio). Una vez dentro de la categoría, filtrar
por ubicación "Capital Federal", de la manera que se muestra en la siguiente
imagen.

Seleccionar una publicación al azar y validar que los datos de la publicación
accedida coincidan con los datos que se habían mostrado de ella en el listado de
productos.

************************************************************Test2***********************************************************************/	

public class Ejercicio2 {
	
	private String categoria, subCategoria, filtro; 
	
	@Test
	public void busquedaDeProductoAleatorioCondicionesFiltradoPorCapitalFederalResultadoOK() throws InterruptedException {
	categoria= "Hogar y Electrodomésticos"; //Es posible que tenga que ocurra un error por el uso de acentos
	subCategoria= "Climatización"; //Es posible que tenga que ocurra un error por el uso de acentos 
	filtro="Capital Federal";
	String localDir = System.getProperty("user.dir");
	System.setProperty("webdriver.chrome.driver", localDir+"\\dependencias\\chromedriver.exe");
	WebDriver driver = new ChromeDriver();
	MercadoLibre mercadoLibre = new MercadoLibre();
	mercadoLibre.navegarPorCategorias(categoria, subCategoria, driver);
	boolean resultado= mercadoLibre.busquedaDeProductoAleatorio(filtro, driver);
	Assert.assertTrue(resultado);
	if (resultado) {
		System.out.println("La validacion del producto selecionado aleatoriamente finalizo sin errores");
	}
	driver.quit();
	}
}
