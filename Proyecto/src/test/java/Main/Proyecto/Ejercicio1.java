/*************************************************************Prueba*********************************************************************
*Desarrollador: Joel Alejandro Pino
*
*Ultima Modificacion: 20/11/2019
*
*****************************************************************************************************************************************/
package Main.Proyecto;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import Entidades.MercadoLibre;

/*******************************************************Ejercicio 1**********************************************************************
Realizar 5 (cinco) casos de prueba de navegabilidad de categorías de MercadoLibre
validando, una vez que se accede a ellas, el título de la categoría accedida y que se
muestre el total de resultados. Las categorías que deberán automatizarse son:
*Categorías * Hogar y Electrodomésticos * Climatización
* Categorías * Tecnología * Celulares y Smartphones
* Categorías * Belleza y Cuidado Personal * Perfumes Importados
* Categorías * Herramientas e Industria * Industria Textil
* Categorías * Juguetes y Bebés * Cuarto del Bebé

Una vez dentro de la categoría, los puntos solicitados a validar son los indicados en
la imagen siguiente:
*Nombre y resultados

************************************************************Test1***********************************************************************/

public class Ejercicio1 {
	
	private String categoria, subCategoria; 
	
	@Test
	public void navegarPorCategoriasCondicionesCategoriaHogarYElectrodomesticosSubCategoriaClimatizacionResultadoOK() throws InterruptedException {
		categoria= "Hogar y Electrodomésticos"; //Es posible que ocurra un error por el uso de acentos.
		subCategoria ="Climatización"; //Es posible que ocurra un error por el uso de acentos.
		String localDir = System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver", localDir+"\\dependencias\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		MercadoLibre mercadoLibre = new MercadoLibre();
		boolean resultado= mercadoLibre.navegarPorCategorias(categoria, subCategoria, driver);
		Assert.assertTrue(resultado);
		driver.quit();
	}
	
	@Test
	public void navegarPorCategoriasCondicionesCategoriaTecnologiaSubCategoriaCelularesYSmartphonesResultadoOK() throws InterruptedException {
		categoria= "Tecnología"; //Es posible que ocurra un error por el uso de acentos.
		subCategoria ="Celulares y Smartphones";
		String localDir = System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver", localDir+"\\dependencias\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		MercadoLibre mercadoLibre = new MercadoLibre();
		boolean resultado= mercadoLibre.navegarPorCategorias(categoria, subCategoria, driver);
		Assert.assertTrue(resultado);
		driver.quit();
	}
	
	@Test
	public void navegarPorCategoriasCondicionesCategoriaHerramientasEIndustriaSubCategoriaIndustriaTextilResultadoOK() throws InterruptedException {
		categoria= "Herramientas e Industria";
		subCategoria ="Industria Textil";
		String localDir = System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver", localDir+"\\dependencias\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		MercadoLibre mercadoLibre = new MercadoLibre();
		boolean resultado= mercadoLibre.navegarPorCategorias(categoria, subCategoria, driver);
		Assert.assertTrue(resultado);
		driver.quit();
	}
	
	@Test
	public void navegarPorCategoriasCondicionesCategoriaJuguetesYBebesSubCategoriaCuartoDelBebeResultadoOK() throws InterruptedException {
		categoria= "Juguetes y Bebés"; //Es posible que ocurra un error por el uso de acentos.
		subCategoria ="Cuarto del Bebé"; //Es posible que ocurra un error por el uso de acentos.
		String localDir = System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver", localDir+"\\dependencias\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		MercadoLibre mercadoLibre = new MercadoLibre();
		boolean resultado= mercadoLibre.navegarPorCategorias(categoria, subCategoria, driver);
		Assert.assertTrue(resultado);
		driver.quit();
	}
	
	@Test
	public void navegarPorCategoriasCondicionesCategoriaBellezaYCuidadoPersonalSubCategoriaPerfumesImportadosResultadoError() throws InterruptedException {
		categoria= "Belleza y Cuidado Personal";
		subCategoria ="Perfumes Importados";
		String localDir = System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver", localDir+"\\dependencias\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		MercadoLibre mercadoLibre = new MercadoLibre();
		boolean resultado= mercadoLibre.navegarPorCategorias(categoria, subCategoria, driver);
		Assert.assertFalse(resultado);
		driver.quit();
	}
	
}
