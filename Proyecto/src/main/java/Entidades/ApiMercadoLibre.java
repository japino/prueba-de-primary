package Entidades;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.WebDriver;

public class ApiMercadoLibre {
	private int totalProducto = 0;
	private int limite = 0;

	// Obtiene la cantidad de stock del atributo "paging.total" del json que
	// devuelve la consulta de la api, luego busca el producto en la web.
	// Obtiene la cantidad del producto que se visualiza en la web y compara ambas
	// cantidades.
	public boolean verificarCantidadDeStock(String url, String nombreProducto, WebDriver driver)
			throws UnirestException, InterruptedException {
		MercadoLibre mercadoLibre = new MercadoLibre();
		int cantidadProductoConsultadoPorWeb = mercadoLibre.buscarUnProducto(nombreProducto, driver);
		HttpResponse<String> response = Unirest.get(url).asString();
		String json = response.getBody();
		JSONObject obj = new JSONObject(json);
		totalProducto = Integer.parseInt(obj.getJSONObject("paging").get("total").toString());

		if (totalProducto != cantidadProductoConsultadoPorWeb) {
			System.out.println("La cantidad del producto buscado mediante la Api es: "+ totalProducto);
			System.out.println("La cantidad del producto buscado mediante la Api es: "+ cantidadProductoConsultadoPorWeb);
			System.out.println("No coinciden las cantidades de productos consultados");
			return false;
		}
		return true;
	}
	
	// Obtiene el tamaño del paginado del atributo "paging.limit" del json que
	// devuelve la consulta de la api.
	// Luego obtiene la cantidad de objetos devueltos por el json a travez del
	// metodo ("results").length()
	// Finalmente compara si las cantidades coinciden.
	public boolean validarLimiteDelPaginado(String url) throws UnirestException {
		HttpResponse<String> response = Unirest.get(url).asString();
		String json = response.getBody();
		JSONObject obj = new JSONObject(json);
		limite = Integer.parseInt(obj.getJSONObject("paging").get("limit").toString());
		int cantidadDeProductosEncontrados = obj.getJSONArray("results").length();
		System.out.println("El tama�o del paginado es: "+ limite);
		System.out.println("La cantidad de objetos devueltos en el json es: "+cantidadDeProductosEncontrados);
		if (cantidadDeProductosEncontrados != limite) {
			System.out.println("No coinciden el limite del paginado con la cantidad de productos encontrados");
			return false;
		}
		return true;
	}

	// Este metodo realiza las siguientes acciones en el siguiente orden:
	// 1�. Genera un nuemero aleatorio del 1 al 50.
	// 2�. Realiza una consulta a la api sobre un listado de productos.
	// 3�. Obtiene el ID de un producto en particular.
	// 4�. Genera un objeto de tipo Producto llamado productoAleatorio.
	// 5�. Consulta el producto mediante el sitio web y obtiene un nuevo objeto
	// Producto llamado productoEncontradoEnWeb.
	// 6�. Compara ambos objetos e imprime si encuentra alguna diferencia.
	public boolean validarProductoConsultadoAlAzar(String url, WebDriver driver)
			throws UnirestException, InterruptedException {
		boolean validacion = true;
		MercadoLibre mercadoLibre = new MercadoLibre();
		int numeroAleatorio = mercadoLibre.generarNumeroAleatorio(1, 50);
		HttpResponse<String> response = Unirest.get(url).asString();
		String json = response.getBody();
		JSONObject obj = new JSONObject(json);
		JSONArray arr = obj.getJSONArray("results");
		for (int i = 0; i < arr.length(); i++) {
			if (i == numeroAleatorio) {
				String titulo = (arr.getJSONObject(i).get("title").toString()).trim();
				float precio = Float.parseFloat((arr.getJSONObject(i).get("price").toString()));
				boolean flagMercadoPago = Boolean
						.parseBoolean(arr.getJSONObject(i).get("accepts_mercadopago").toString());
				String moneda = arr.getJSONObject(i).get("currency_id").toString();
				boolean flagEnvioGratis = Boolean
						.parseBoolean(arr.getJSONObject(i).getJSONObject("shipping").get("free_shipping").toString());
				String link = arr.getJSONObject(i).get("permalink").toString();
				System.out.println("El ID del producto seleccionado es: " + arr.getJSONObject(i).get("id").toString());
				Producto productoAleatorio = new Producto(titulo, precio, flagMercadoPago, moneda, flagEnvioGratis,
						link);
				System.out.println("El link del producto seleccionado es: " + link);
				Producto productoEncontradoEnWeb = mercadoLibre.buscarDetallesDeUnProducto(productoAleatorio.getLink(),
						driver);
				if (!productoAleatorio.getTitulo().equals(productoEncontradoEnWeb.getTitulo())) {
					// Aveces falla ya que en la api envia el titulo del producto con Mayusculas o
					// minusculas, espacios y/o palabras
					// que difieren del titulo visualizado en la web.
					System.out.println("El titulo del producto no coincide con el titulo encontrado en la web");
					System.out.println(productoAleatorio.getTitulo());
					System.out.println(productoEncontradoEnWeb.getTitulo());
					validacion = false;
				}
				if (productoAleatorio.getPrecio() != productoEncontradoEnWeb.getPrecio()) {
					System.out.println("El precio del producto no coincide con el encontrado en la web");
					System.out.println(productoAleatorio.getPrecio());
					System.out.println(productoEncontradoEnWeb.getPrecio());
					validacion = false;
				}
				if (productoAleatorio.getFlagMercadoPago() != productoEncontradoEnWeb.getFlagMercadoPago()) {
					System.out.println("Uno de los productos no acepta mercado Pago");
					System.out.println(productoAleatorio.getFlagMercadoPago());
					System.out.println(productoEncontradoEnWeb.getFlagMercadoPago());
					validacion = false;
				}
				if (!productoAleatorio.getMoneda().equals(productoEncontradoEnWeb.getMoneda())) {
					System.out.println("El tipo de moneda del producto no coincide con el titulo encontrado en la web");
					validacion = false;
				}
				if (productoAleatorio.getFlagEnvioGratis() != productoEncontradoEnWeb.getFlagEnvioGratis()) {
					System.out.println("Uno de los productos no cuenta con envios gratis");
					validacion = false;
				}
			}
		}
		if (!validacion)
			return false;

		System.out.println("La validacion de los productos finalizo correctamente");
		return true;
	}
}
