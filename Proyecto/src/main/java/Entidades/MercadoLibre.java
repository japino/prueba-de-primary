
/*************************************************************Prueba*********************************************************************
*Desarrollador: Joel Alejandro Pino
*
*Ultima Modificacion: 20/11/2019
*
*****************************************************************************************************************************************/
/********************************************************Aclaraciones*********************************************************************
Esta clase contiene todos los metodos que se utilizan para la automatizacion de las pantallas correspondientes a la web de mercadolibre, 
esta programada para ser hacer pruebas parametrizadas.
******************************************************************************************************************************************/
package Entidades;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import java.util.Random;

public class MercadoLibre {
	// Esta funcion permite la navegacion dentro de la pagina, accediendo a las
	// distintas categorias y subcategorias.
	// Imprime las categorias encontradas y la cantidad de objectos a la venta.
	// Realiza las validaciones de las subcategorias e imprime un mensaje en caso de
	// no encontrar una categoria.'
	public boolean navegarPorCategorias(String categoria, String subcategoria, WebDriver driver)
			throws InterruptedException {
		driver.get("https://www.mercadolibre.com.ar/");
		waitForElement(".nav-menu-categories-link", 3, 2000, driver);
		driver.findElement(By.cssSelector("a[class='nav-menu-categories-link']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//a[contains(text(),'" + categoria + "')]")).click();
		Thread.sleep(2000);
		try {
			driver.findElement(By.xpath("//a[contains(text(),'" + subcategoria + "')]")).click();
			String subcategoriaBuscada = driver.findElement(By.xpath("//h1[contains(@class,'breadcrumb__title')]"))
					.getText();
			System.out.println("Categoria a validar: " + subcategoria);
			if (subcategoria.equals(subcategoriaBuscada)) {
				System.out.println("La categoria " + subcategoriaBuscada + " se valido correctamente");
			} else {
				System.out.println("No coincide la subcategoria encontrada: " + subcategoria);
				return false;
			}
		} catch (Exception errorCategoriaNoEncontrada) {
			System.out.println("No se encontro la subcategoria: " + subcategoria);
			return false;
		}
		return true;
	}

	// La funcion waitForElement se utiliza para esperar a que el elemento buscado
	// aparesca dentro del DOM HTML.
	// Esta funcion esta parametrizada con el fin de adecuarse al comportamiento de
	// distintos elementos.
	// Solo recibe elementos CssSelector.
	public void waitForElement(String elementoCssSelector, int iteraciones, int tiempo, WebDriver driver) {
		int iteracion = 0;
		do {
			try {
				driver.findElement(By.cssSelector(elementoCssSelector)).isEnabled();
				Thread.sleep(tiempo);
				break;
			} catch (Exception errorElementoNoEncontrado) {
				System.out.println("No se encontro el elemento de referencia:" + elementoCssSelector);
			}
			iteracion++;
		} while (iteracion == iteraciones);
	}

	// Busca un producto aleatorio en la web de mercado libre.
	// Luego guarda el nombre y el precio del producto mostrado en una lista y
	// despues lo compara con el mismo pero en la pantalla de detalles del producto.
	public boolean busquedaDeProductoAleatorio(String valorFiltro, WebDriver driver) throws InterruptedException {
		float precioProductoAValidarFloat = 0;
		float precioProductoSeleccionadoFloat = 0;
		driver.findElement(By.xpath("//span[contains(@class,'filter-name')][contains(text(),'" + valorFiltro + "')]"))
				.click();
		Assert.assertEquals(driver.findElement(By.xpath("//div[contains(text(),'Capital Federal')]")).getText(),
				valorFiltro);
		int cantidadDeElementos = driver.findElements(By.xpath("//ol[@id='searchResults']/*")).size();
		int elementoAleatorio = generarNumeroAleatorio(1, cantidadDeElementos);
		String tituloProductoAValidar = driver
				.findElement(By.xpath("/html[1]/body[1]/main[1]/div[2]/div[1]/section[1]/ol[1]/li[" + elementoAleatorio
						+ "]/div[1]/div[2]/div[1]/h2[1]/a[1]/span[1]"))
				.getText();
		System.out.println("El producto aleatorio seleccionado es: " + tituloProductoAValidar);
		String precioProductoAValidar = driver
				.findElement(By.xpath("/html[1]/body[1]/main[1]/div[2]/div[1]/section[1]/ol[1]/li[" + elementoAleatorio
						+ "]/div[1]/div[2]/div[1]/div[1]/*"))
				.getText();
		precioProductoAValidar = precioProductoAValidar.trim();
		String moneda = driver.findElement(By.xpath("/html[1]/body[1]/main[1]/div[2]/div[1]/section[1]/ol[1]/li["
				+ elementoAleatorio + "]/div[1]/div[2]/div[1]/div[1]/div[1]/span[1]")).getText();
		precioProductoAValidar = precioProductoAValidar.replace(moneda, "");
		precioProductoAValidar = precioProductoAValidar.replace(".", "");
		precioProductoAValidarFloat = Float.parseFloat(precioProductoAValidar);

		System.out.println("El precio del producto aleatorio seleccionado es: " + precioProductoAValidarFloat);
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html[1]/body[1]/main[1]/div[2]/div[1]/section[1]/ol[1]/li[" + elementoAleatorio
				+ "]/div[1]/div[2]/div[1]/h2[1]/a[1]/span[1]")).click();
		String nombreProductoSeleccionado = driver.findElement(By.xpath("//h1[contains(@class,'item-title__primary')]"))
				.getText();

		// Obtiene el precio, lo parsea en un float y lo guarda en una variable.
		precioProductoSeleccionadoFloat = Float.parseFloat(driver.findElement(By.xpath(
				"//fieldset[contains(@class,'item-price')]//span[contains(@class,'price-tag-symbol')][contains(text(),'"
						+ moneda + "')]"))
				.getAttribute("content"));
		System.out.println(nombreProductoSeleccionado);
		System.out.println(precioProductoSeleccionadoFloat);
		if (tituloProductoAValidar.equals(nombreProductoSeleccionado)
				&& precioProductoAValidarFloat == precioProductoSeleccionadoFloat) {
			System.out.println("El precio y el producto se validaron correctamente.");
		} else {
			System.out.println("El precio y el producto no coinciden.");
			return false;
		}
		return true;
	}

	// Metodo que genera un numero aleatorio que se utiliza otros metodos.
	public int generarNumeroAleatorio(int min, int max) {
		if (min >= max) {
			throw new IllegalArgumentException("El numero maximo no puede ser menor que el minimo");
		}
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}
	
	// Busca un producto en la web de mercado libre y retorna la cantidad de
	// existencias del mismo.
	public int buscarUnProducto(String nombreProducto, WebDriver driver) throws InterruptedException {
		int cantidadStockProducto;
		driver.get("https://www.mercadolibre.com.ar/");
		driver.findElement(By.xpath("//input[@placeholder='Buscar productos, marcas y más…']")) // Es posible que ocurra
																								// un error por el uso
																								// de acentos.
				.sendKeys(nombreProducto);
		driver.findElement(By.xpath("//button[@class='nav-search-btn']")).click();
		Thread.sleep(2000);
		String resultados = driver.findElement(By.xpath("//div[contains(@class,'quantity-results')]")).getText();
		resultados = resultados.replace(" resultados", "");
		resultados = resultados.replace(".", "");
		cantidadStockProducto = Integer.parseInt(resultados);
		driver.quit();
		return cantidadStockProducto;
	}

	// Busca los detalles de un producto en la web y retorna un Producto.
	public Producto buscarDetallesDeUnProducto(String linkProducto, WebDriver driver) throws InterruptedException {
		driver.get(linkProducto);
		Thread.sleep(2000);
		String titulo;
		String simboloMoneda = "$";
		boolean flagEnvioGratis = false;
		boolean flagMercadoPago = false;
		float precio = 0;
		String urlMetodosDePago;
		String contenedorHTMLEnvios;
		String[] palabras;
		// Obtiene el titulo de las posibles clases y lo guarda en una variable.
		try {
			titulo = driver.findElement(By.cssSelector(".ui-pdp-title")).getText();
		} catch (Exception errorClaseNoEncontrada) {
			titulo = driver.findElement(By.xpath("//h1[contains(@class,'item-title__primary')]")).getText();
		}
		// Obtiene el tipo de moneda, lo transforma y lo guarda en una variable.
		String moneda = driver.findElement(By.cssSelector(".price-tag-symbol")).getText();
		if (moneda.equals("$")) {
			simboloMoneda = "$"; // Guarda su simbolo para usarlo en el siguiente objeto.
			moneda = "ARS";// Se transforma la variable para que coincida con la extraida de la API. Se
							// entiende que hay una transformacion a nivel servidor.
		}
		if (moneda.equals("U$S")) {
			simboloMoneda = "U$S";
			moneda = "USD";
		}
		// Obtiene el precio, lo parsea en un float y lo guarda en una variable.
		precio = Float.parseFloat(driver.findElement(By.xpath(
				"//fieldset[contains(@class,'item-price')]//span[contains(@class,'price-tag-symbol')][contains(text(),'"
						+ simboloMoneda + "')]"))
				.getAttribute("content"));
		// Obtiene el texto de los elementos que contienen informacion sobre el envio.
		// Busca la palabra clave "gratis"
		try {
			contenedorHTMLEnvios = driver.findElement(By.cssSelector(
					"div.ui-pdp div.ui-pdp-container.ui-pdp-container--pdp:nth-child(2) div.ui-pdp-container__row.ui-pdp--relative.ui-pdp-with--separator--fluid.pb-40:nth-child(1) div.ui-pdp-container__col.col-1.ui-pdp-container--column-right.mt-16.pr-16:nth-child(2) div.ui-pdp-container__row:nth-child(1) form.ui-pdp-buybox div.andes-tooltip__trigger:nth-child(2) div.ui-pdp-media.ui-pdp-shipping.ui-pdp-shipping--md.mt-20.ui-pdp-color--GREEN div.ui-pdp-media__body > h2.ui-pdp-media__title"))
					.getText();
			palabras = contenedorHTMLEnvios.split(" ");
			for (String i : palabras) {
				if (i.equals("gratis")) {
					flagEnvioGratis = true;
				}
			}
		} catch (Exception errorClaseNoEncontrada) {

			try {
				contenedorHTMLEnvios = driver.findElement(By.xpath("//span[contains(@class,'green')]")).getText();
				palabras = contenedorHTMLEnvios.split(" ");
				for (String i : palabras) {
					if (i.equals("gratis")) {
						flagEnvioGratis = true;
					}
				}

			} catch (Exception errorClaseGreenNoEncontrada) {
				flagEnvioGratis = false;
			}
		}
		// Obtiene la URL de los iframe que contiene la informacion con los tipos de
		// pagos, navega sobra la URL extraida y busca la palabra mercado pago.
		try {
			driver.findElement(By.xpath("//a[contains(text(),'Más información')]")).click(); // Es posible que ocurra un
																								// error por el uso de
																								// acentos.
			Thread.sleep(3000);
			urlMetodosDePago = driver.findElement(By.xpath("//iframe[@class='modal-iframe']")).getAttribute("src");
			driver.navigate().to(urlMetodosDePago);
			Thread.sleep(2000);
			flagMercadoPago = true;
		} catch (Exception errorClaseModalIframeNoEncontrada) {
			try {
				driver.findElement(By.xpath("//a[contains(text(),'Más información')]")).click(); // Es posible que
																									// ocurra un error
																									// por el uso de
																									// acentos.
				Thread.sleep(3000);
				urlMetodosDePago = driver.findElement(By.xpath("//iframe[@class='ui-pdp-iframe']")).getAttribute("src");
				driver.navigate().to(urlMetodosDePago);
				Thread.sleep(2000);
				flagMercadoPago = true;
			} catch (Exception errorClaseUipPdpIframeNoEncontrada) {
				flagMercadoPago = false;
			}
		}
		// retorna un objeto producto para compara con el generado por la api.
		Producto producto = new Producto(titulo, precio, flagMercadoPago, moneda, flagEnvioGratis, linkProducto);
		driver.quit();
		return producto;
	}
}
