package Entidades;

public class Producto {

	private String titulo;
	private float precio;
	private boolean flagMercadoPago;
	private String moneda;
	private boolean flagEnvioGratis;
	private String link;

	public Producto(String titulo, float precio, boolean flagMercadoPago, String moneda, boolean flagEnvioGratis,
			String link) {
		this.titulo = titulo;
		this.precio = precio;
		this.flagMercadoPago = flagMercadoPago;
		this.moneda = moneda;
		this.flagEnvioGratis = flagEnvioGratis;
		this.link = link;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public boolean getFlagEnvioGratis() {
		return flagEnvioGratis;
	}

	public void setFlagEnvioGratis(boolean flagEnvioGratis) {
		this.flagEnvioGratis = flagEnvioGratis;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public boolean getFlagMercadoPago() {
		return flagMercadoPago;
	}

	public void setFlagMercadoPago(boolean flagMercadoPago) {
		this.flagMercadoPago = flagMercadoPago;
	}

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	@Override
	public String toString() {
		return this.getTitulo() + " " + this.getPrecio() + " " + this.getMoneda() + " " + this.getFlagMercadoPago()
				+ " " + this.getFlagEnvioGratis();
	}

}
